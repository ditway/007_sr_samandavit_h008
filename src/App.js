import { useState } from "react";
import {
  Badge,
  Button,
  Card,
  Col,
  Container,
  Form,
  Row,
  Table,
} from "react-bootstrap";
import userLogo from "./images/user.png";
import "./App.css";
function App() {
  const [validated, setValidated] = useState(false);
  const [selected, setSelected] = useState([]);
  const [users, setUsers] = useState([]);
  function onSubmit(event) {
    event.preventDefault();
    const form = event.currentTarget;
    if (form.checkValidity()) {
      const user = {
        id: generateID(),
        username: form.username.value,
        gender: form.gender.value,
        email: form.email.value,
        password: form.password.value,
      };
      onAddUser(user);
      form.reset();
    }
    setValidated(true);
  }
  function generateID() {
    if (users.length === 0) return 1;
    return Math.max(...users.map((x) => x.id)) + 1;
  }
  function onAddUser(user) {
    setUsers([...users, user]);
  }
  function onSeletedRow(user) {
    const isPresent = selected.indexOf(user) > -1;
    if (isPresent) {
      setSelected([...selected.filter((x) => x.id !== user.id)]);
    } else setSelected([user, ...selected]);
  }
  function getRowClassName(user) {
    const isPresent = selected.indexOf(user) > -1;
    if (isPresent) return "cursor-pointer active-row";
    else return "cursor-pointer";
  }
  function onDeleteUser() {
    if (selected.length > 0)
      setUsers([...users.filter((x) => !selected.includes(x))]);
    setSelected([]);
  }
  function renderForm() {
    return (
      <Col className="col-lg-5 col-8 mb-4">
        <Card>
          <Card.Header>
            <div className="d-flex flex-column align-items-center">
              <img className="mb-3" style={{ width: 60 }} src={userLogo} />
              <h4>Create Account</h4>
            </div>
          </Card.Header>
          <Card.Body>
            <Form noValidate validated={validated} onSubmit={onSubmit}>
              <Form.Group controlId="username">
                <Form.Label>Username</Form.Label>
                <Form.Control type="text" required />
                <Form.Control.Feedback type="invalid">
                  Username is required
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="gender">
                <Form.Label>Gender</Form.Label>
                <div className="d-flex">
                  <Form.Check
                    name="gender"
                    type="radio"
                    label="Male"
                    value="Male"
                    required
                    className="mr-3"
                  />
                  <Form.Check
                    name="gender"
                    type="radio"
                    value="Female"
                    label="Female"
                    required
                  />
                </div>
              </Form.Group>
              <Form.Group controlId="email">
                <Form.Label>Email</Form.Label>
                <Form.Control type="text" required />
                <Form.Control.Feedback type="invalid">
                  Email is required
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" required />
                <Form.Control.Feedback type="invalid">
                  Password is required
                </Form.Control.Feedback>
              </Form.Group>
              <Button type="submit">Submit</Button>
            </Form>
          </Card.Body>
        </Card>
      </Col>
    );
  }
  function renderTable() {
    return (
      <Col className="col-lg-7 col-12">
        <h4>Table Accounts</h4>
        <Table bordered>
          <thead>
            <tr>
              <th>#</th>
              <th>Username</th>
              <th style={{ width: 250 }}>Email</th>
              <th>Gender</th>
            </tr>
          </thead>
          <tbody>
            {users.map((item, index) => {
              return (
                <tr
                  onClick={() => onSeletedRow(item)}
                  className={getRowClassName(item)}
                  key={index}
                >
                  <td>{item.id}</td>
                  <td>{item.username}</td>
                  <td>{item.email}</td>
                  <td>{item.gender}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        {selected.length > 0 ? (
          <div className="d-flex align-items-center">
            <Button variant="danger" onClick={onDeleteUser}>
              Delete
            </Button>
            <Badge
              className="ml-2"
              variant="info"
            >{`${selected.length} selected`}</Badge>
          </div>
        ) : undefined}
      </Col>
    );
  }
  return (
    <Container>
      <div className="p-3 d-flex justify-content-between mb-5">
        <h5 className="h6">KSHRD Student</h5>
        <h6 className="text-secondary">
          Signed in as: <a href="#">Sam an davit</a>
        </h6>
      </div>
      <Row className="justify-content-center">
        {renderForm()}
        {renderTable()}
      </Row>
    </Container>
  );
}

export default App;
